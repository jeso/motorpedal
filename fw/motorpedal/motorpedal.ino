#define PEDAL_POT_APIN 0    // Analog pin of pedal potentiometer
#define MOTOR_PWM_PIN  6    // PWM Output pin
#define TIME_CALIB_MAX 5    // Calibration time
#define PEDAL_FLATNESS 5   // Percent of inactive area (up and down)

int ADCValue;           // Current value
int ADCValue_CalibMin;  // Pedal min/max calibration
int ADCValue_CalibMax;
int pwmValue;           // Pedal recalculated value to percent from min/max
int timeCalibStart;     // time when calibration started

float fPedalFlatnessCoeffLo;    // Coefficient for pedal flatness
float fPedalFlatnessCoeffHi;    // Coefficient complement for pedal flatness
float fPedalFlatnessExpand;   // Expander for pedal flatness
float fScaling;

void setup() {

  // Debug console setup
  Serial.begin(9600);

  // ADC Setup
  analogReference(DEFAULT);

  // Pedal calibration sequence
  Serial.println("Calibration start");
  timeCalibStart = millis();
  ADCValue_CalibMin = 1023;
  ADCValue_CalibMax = 0;
  while((millis() - timeCalibStart) < (TIME_CALIB_MAX*1000)){
    ADCValue = analogRead(PEDAL_POT_APIN);
    if(ADCValue < ADCValue_CalibMin){
      ADCValue_CalibMin = ADCValue;
      Serial.print(".");
    }
    if(ADCValue > ADCValue_CalibMax){
      ADCValue_CalibMax = ADCValue;
      Serial.print(".");
    }
  }

  // Flatness coefficient calculation
  fPedalFlatnessCoeffLo = (PEDAL_FLATNESS * 0.01);
  fPedalFlatnessCoeffHi = 1.0 - fPedalFlatnessCoeffLo;
  fPedalFlatnessExpand = (PEDAL_FLATNESS * 0.01 * 2.0) + 1.0;

  /*
  Serial.println("Calibration: ");
  Serial.println(ADCValue_CalibMin);
  Serial.println(ADCValue_CalibMax);
  Serial.println(fPedalFlatnessCoeffLo);
  Serial.println(fPedalFlatnessCoeffHi);
  Serial.println(fPedalFlatnessExpand);
  */ //DEBUG
}

void loop() {

  // Readout
  ADCValue = analogRead(PEDAL_POT_APIN);

  // Saturation
  if(ADCValue < ADCValue_CalibMin) ADCValue = ADCValue_CalibMin;
  if(ADCValue > ADCValue_CalibMax) ADCValue = ADCValue_CalibMax;

  // Scaling
  fScaling = (ADCValue - ADCValue_CalibMin);
  fScaling = fScaling / (ADCValue_CalibMax - ADCValue_CalibMin);
  fScaling = fScaling * 255;

  //Serial.print("Post scaling: "); // DEBUG
  //Serial.println(fScaling);       // DEBUG

  // Flat area insertion
  if      (fScaling < (255 * fPedalFlatnessCoeffLo)){
    fScaling = 0;
  }
  else if (fScaling > (255 * fPedalFlatnessCoeffHi)){
    fScaling = 255;
  }
  else {
    fScaling = fScaling - (255 * fPedalFlatnessCoeffLo);
    fScaling = fScaling * fPedalFlatnessExpand;
  }

  // Output
  pwmValue = fScaling;
  //Serial.println(pwmValue); // DEBUG
  analogWrite(MOTOR_PWM_PIN, pwmValue);
}
